// Based off these:
// https://github.com/ppy/osu-web/blob/f47196f91443dbbfefe703cbf8ebc46987ab1c19/app/Libraries/BBCodeFromDB.php
// https://github.com/ppy/osu-web/blob/2d5aa6ef2e936dbea1dc34dd36899e816a64dabf/app/Libraries/BBCodeForDB.php

/**
 * This class converts HTML to BBCode
 */
export class BBCodeConverter {
    /**
     * html document to convert into bbcode
     */
    private htmlDoc: Document

    /**
     * raw quote html that htmlDoc is build with
     */
    private text: string

    /**
     * constructor
     * @param text string representation of the document
     */
    constructor(text: string) {
        this.text = text
        this.htmlDoc = new DOMParser().parseFromString(text, "text/html")
    }

    /**
     * This function starts the parsing process.
     * It starts off with a text representation of the selected HTML.
     * This HTML gets converted into BBCode step by step and eventually returned
     * @returns bbcode string of the selection
     */
    public parse() {
        // end goal bbcode representation, gets started as raw html and slowly converts to bbcode
        let bbCodeString = this.text

        // limit to just forum post in case selection is bigger
        let bbcodeBlock = this.htmlDoc.querySelector("div.bbcode")
        if (bbcodeBlock) {
            bbCodeString = bbcodeBlock.innerHTML
        }

        bbCodeString = this.parseAudio(bbCodeString)
        bbCodeString = this.parseBold(bbCodeString)
        bbCodeString = this.parseBox(bbCodeString)
        bbCodeString = this.parseCentre(bbCodeString)
        bbCodeString = this.parseCode(bbCodeString)
        bbCodeString = this.parseColor(bbCodeString)
        bbCodeString = this.parseHeading(bbCodeString)
        bbCodeString = this.parseImage(bbCodeString)
        bbCodeString = this.parseItalic(bbCodeString)
        bbCodeString = this.parseList(bbCodeString)
        bbCodeString = this.parseNotice(bbCodeString)
        bbCodeString = this.parseQuote(bbCodeString)
        bbCodeString = this.parseSize(bbCodeString)
        bbCodeString = this.parseSmilies(bbCodeString)
        bbCodeString = this.parseSpoiler(bbCodeString)
        bbCodeString = this.parseStrike(bbCodeString)
        bbCodeString = this.parseUnderline(bbCodeString)
        bbCodeString = this.parseUrl(bbCodeString)
        bbCodeString = this.parseYoutube(bbCodeString)

        bbCodeString = bbCodeString.replaceAll("<br>", "\n")
        bbCodeString = bbCodeString.replaceAll("<br />", "\n")

        return bbCodeString
    }

    private parseAudio(text: string) {
        // TODO: Use raw post content to get actual source URL instead of the proxied i.ppy.sh source (should be possible by using the indicies in the post)

        let audioBlocks = this.htmlDoc.querySelectorAll<HTMLDivElement>("div.audio-player")

        Array.from(audioBlocks).forEach((audioBlock: HTMLDivElement) => {
            let audioSource = audioBlock.dataset.audioUrl

            text = text.replace(audioBlock.outerHTML, `[audio]${audioSource}[/audio]`)
        })

        // Audio blocks are really hard to properly select, so we'll just remove any partial or invalid selections
        audioBlocks = this.htmlDoc.querySelectorAll("div[class*='audio']")
        Array.from(audioBlocks).forEach((audioBlock: HTMLDivElement) => {
            text = text.replace(audioBlock.outerHTML, "")
        })

        return text
    }

    private parseBold(text: string) {
        text = text.replaceAll("<strong></strong>", "")
        text = text.replaceAll("<strong>", "[b]")
        text = text.replaceAll("</strong>", "[/b]")

        return text
    }

    private parseBox(text: string) {
        // TODO: This doesn't work properly when selecting a closed box + other content
        // TODO: this doesn't always get the title

        let boxes = this.htmlDoc.querySelectorAll<HTMLDivElement>("div.bbcode-spoilerbox")

        Array.from(boxes).forEach((box: HTMLDivElement) => {
            let boxTitle = box.querySelector<HTMLSpanElement>("button.bbcode-spoilerbox__link")?.innerText
            if(boxTitle == undefined){ // prevent empty boxes showing "undefined" due to string conversion
                boxTitle = ""
            }
            let boxContent = box.querySelector<HTMLDivElement>("div.bbcode-spoilerbox__body")

            if (boxContent) {
                text = text.replace(box.outerHTML, `[box=${boxTitle}]${boxContent.innerHTML}[/box]`)
            } else {
                text = text.replace(box.outerHTML, `[box=${boxTitle}][/box]`)
            }
        })

        // Check for any remaining spoilerbox link tags that were missing parent elements
        boxes = this.htmlDoc.querySelectorAll<HTMLDivElement>("button.bbcode-spoilerbox__link")
        Array.from(boxes).forEach((box: HTMLDivElement) => {
            let boxTitle = box.innerText
            let boxContent = <HTMLDivElement | null>box.nextSibling

            if (boxContent) {
                text = text.replace(box.outerHTML, `[box=${boxTitle}]${boxContent?.innerHTML}[/box]`)
                text = text.replace(boxContent.outerHTML, "") // Remove box content since it was included in the previous replacement
            } else {
                text = text.replace(box.outerHTML, `[box=${boxTitle}][/box]`)
            }
        })

        // Check if any box icons were selected
        let boxIcons = this.htmlDoc.querySelectorAll<HTMLSpanElement>("span.bbcode-spoilerbox__link-icon")
        Array.from(boxIcons).forEach((boxIcon: HTMLSpanElement) => {
            // Just remove them for now, if only the icon is selected then it is missing content
            text = text.replace(boxIcon.outerHTML, "")
        })

        text = text.replaceAll("[box=][/box]", "")

        return text
    }

    private parseCentre(text: string) {
        text = text.replaceAll("<center></center>", "")
        text = text.replaceAll("<center>", "[centre]")
        text = text.replaceAll("</center>", "[/centre]")

        return text
    }

    private parseCode(text: string) {
        let codeBlocks = this.htmlDoc.getElementsByTagName("pre")

        Array.from(codeBlocks).forEach((codeBlock: HTMLPreElement) => {
            text = text.replace(codeBlock.outerHTML, `[code]${codeBlock.innerText}[/code]`)
        })

        return text
    }

    private parseColor(text: string) {
        // Get all spans with the inline style "color"
        let colorElements = this.htmlDoc.querySelectorAll("span[style*='color:']")

        Array.from(colorElements).forEach((colorElement: Element) => {
            let color = colorElement.getAttribute("style")?.replace("color:", "").replace(";", "")

            text = text.replace(colorElement.outerHTML, `[color=${color}]${colorElement.innerHTML}[/color]`)
        })

        return text
    }

    private parseHeading(text: string) {
        text = text.replaceAll("<h2></h2>", "")
        text = text.replaceAll("<h2>", "[heading]")
        text = text.replaceAll("</h2>", "[/heading]")
        return text
    }

    private parseItalic(text: string) {
        text = text.replaceAll("<em></em>", "")
        text = text.replaceAll("<em>", "[i]")
        text = text.replaceAll("</em>", "[/i]")

        return text
    }

    private parseImage(text: string) {
        // TODO: Use raw post content to get actual source URL instead of the proxied i.ppy.sh source (should be possible by using the indicies in the post)
        // TODO: seems a bit confused with the image gallery triggering the popup

        let images = this.htmlDoc.querySelectorAll<HTMLSpanElement>("span.proportional-container")

        if (images.length == 0) {
            images = this.htmlDoc.querySelectorAll<HTMLSpanElement>("span.proportional-container__height")
        }

        Array.from(images).forEach((image: HTMLSpanElement) => {
            let imageChild = image.querySelector("img")
            let imageUrl = imageChild?.src

            if (imageUrl) {
                text = text.replace(image.outerHTML, `[img]${imageUrl}[/img]`)
            } else {
                text = text.replace(image.outerHTML, "")
            }
        })

        return text
    }

    private parseList(text: string) {
        text = text.replaceAll("<li></li>", "")
        if (text.startsWith("<li>")) {
            text = '<ol class="unordered">' + text + "</ol>"

            let parser = new DOMParser()
            this.htmlDoc = parser.parseFromString(text, "text/html")
        }

        let unorderedList = this.htmlDoc.getElementsByTagName("ol")

        Array.from(unorderedList).forEach((ol: HTMLOListElement) => {
            let bbCodeString = "[list=1]\n"
            if (ol.classList.contains("unordered")) {
                bbCodeString = "[list]\n"
            }

            let listItems = ol.getElementsByTagName("li")
            Array.from(listItems).forEach((li: HTMLLIElement) => {
                bbCodeString += `[*]${li.innerHTML}`
            })

            bbCodeString += "[/list]"
            text = text.replace(ol.outerHTML, bbCodeString)
        })

        return text
    }

    private parseNotice(text: string) {
        let notices = this.htmlDoc.getElementsByClassName("well")

        Array.from(notices).forEach((notice: Element) => {
            text = text.replace(notice.outerHTML, `[notice]${notice.innerHTML}[/notice]`)
        })

        return text
    }

    private parseQuote(text: string) {
        // TODO: Fix partially selected inner text not having the "written by" value.
        // Fix only selecting title text not working at all

        // Only the "written by" section of the quote was selected
        // if (text.startsWith("<h4>")) {
        //     text = "<blockquote>" + text + "<blockquote>"

        //     let parser = new DOMParser();
        //     this.htmlDoc = parser.parseFromString(text, 'text/html');
        // }

        let quotes = this.htmlDoc.getElementsByTagName("blockquote")

        Array.from(quotes).forEach((quote: HTMLQuoteElement) => {
            let originalQuote = <HTMLQuoteElement>quote.cloneNode(true)

            let usernameBlock = quote.getElementsByTagName("h4")
            let username = null

            if (usernameBlock && usernameBlock.length > 0) {
                username = usernameBlock[0].innerText.replace(" wrote:", "")
                quote.removeChild(usernameBlock[0])
            }

            if (quote.innerHTML) {
                let finalString = "[quote"

                if (username) {
                    finalString += `="${username.trim()}"`
                }

                finalString += "]"
                finalString += quote.innerHTML
                finalString += "[/quote]"

                text = text.replace(originalQuote.outerHTML, finalString)
            } else {
                text = text.replace(originalQuote.outerHTML, "")
            }
        })

        return text
    }

    private parseSmilies(text: string) {
        let smilies = this.htmlDoc.getElementsByClassName("smiley")

        Array.from(smilies).forEach((smiley: Element) => {
            if (smiley.tagName === "IMG") {
                let imageSmiley = <HTMLImageElement>smiley
                text = text.replace(smiley.outerHTML, imageSmiley.alt + " ")
            }
        })

        return text
    }

    private parseStrike(text: string) {
        text = text.replaceAll("<del></del>", "")
        text = text.replaceAll("<del>", "[s]")
        text = text.replaceAll("</del>", "[/s]")

        return text
    }

    private parseUnderline(text: string) {
        text = text.replaceAll("<u></u>", "")
        text = text.replaceAll("<u>", "[u]")
        text = text.replaceAll("</u>", "[/u]")

        return text
    }

    private parseSpoiler(text: string) {
        // Get all spans with the class spoiler
        let spoilerBlocks = this.htmlDoc.querySelectorAll("span.spoiler")

        Array.from(spoilerBlocks).forEach((spoilerBlock: Element) => {
            text = text.replace(spoilerBlock.outerHTML, `[spoiler]${spoilerBlock.innerHTML}[/spoiler]`)
        })

        return text
    }

    private parseSize(text: string) {
        // Get all spans starting with the class name "size-"
        let sizeBlocks = this.htmlDoc.querySelectorAll("span[class^='size-']")

        Array.from(sizeBlocks).forEach((sizeBlock: Element) => {
            let size = sizeBlock.className.replace("size-", "")

            text = text.replace(sizeBlock.outerHTML, `[size=${size}]${sizeBlock.innerHTML}[/size]`)
        })

        return text
    }

    // Parses URL, Profile, and Email tags in one function
    // TODO: doesnt trigger if just a link is selected and no outside content
    private parseUrl(text: string) {
        Array.from(this.htmlDoc.getElementsByTagName("a")).forEach((anchor: HTMLAnchorElement) => {
            // Remove all comments from within this anchor
            // Remove any previous comments in the element
            let previousSibling = anchor.previousSibling
            if (previousSibling && previousSibling.COMMENT_NODE) {
                text = text.replace(`<!--${previousSibling.nodeValue!}-->`, "")
            }

            // Remove any tailing comments in the element
            let nextSibling = anchor.nextSibling
            if (nextSibling && nextSibling.COMMENT_NODE) {
                text = text.replace(`<!--${nextSibling.nodeValue!}-->`, "")
            }

            if (anchor.classList.contains("js-usercard")) {
                // Profile
                text = text.replace(anchor.outerHTML, `[profile=${anchor.dataset.userId}]${anchor.textContent}[/profile]`)
            } else if (anchor.href.startsWith("mailto:")) {
                // Email
                let email = anchor.href.replace("mailto:", "")
                text = text.replace(anchor.outerHTML, `[email]${email}[/email]`)
            } else {
                // All other URLs
                text = text.replace(anchor.outerHTML, `[url=${anchor.href}]${anchor.innerText}[/url]`)
            }

            // remove links without text content
            text = text.replaceAll(/\[url\=[^\]]*\]\[\/url\]/igm, "")
        })

        return text
    }

    private parseYoutube(text: string) {
        let outerVideos = this.htmlDoc.getElementsByClassName("bbcode__video-box") // If more than just the video itself is selected
        let innerVideos = this.htmlDoc.getElementsByClassName("bbcode__video") // If only the video is selected

        let videos = Array.from(outerVideos).concat(Array.from(innerVideos))

        videos.forEach((video: Element) => {
            let iframe = video.querySelector("iframe")
            let source = iframe?.src

            source = source?.replace("https://www.youtube.com/embed/", "")
            source = source?.replace("?rel=0", "")

            if (source) {
                text = text.replace(video.outerHTML, `[youtube]${source}[/youtube]`)
            } else {
                text = text.replace(video.outerHTML, "")
            }
        })

        return text
    }
}
