import { ensuredSelector } from "../../typescript-lib/src/shared/common"
import { createElement } from "../../typescript-lib/src/shared/dom"
import { BBCodeConverter } from "./bbcode-converter"
import { insertTextIntoBBCodeEditor } from "./index"

/**
 * this function enables quoting of forum posts by text selection
 */
export function quoteSelectedText() {
    if (!location.pathname.includes("forums/topics")) {
        return
    }

    document.querySelectorAll<HTMLElement>(".js-forum-post").forEach((forumPost) => {
        const userName = ensuredSelector<HTMLAnchorElement>(".forum-post-info__row.forum-post-info__row--username", forumPost).innerText
        const postContent = ensuredSelector<HTMLDivElement>("div.forum-post__content.forum-post__content--main", forumPost)

        if (postContent && postContent.dataset.mouseupListenerAdded == null) {
            postContent.addEventListener("click", async (event) => {
                // there can only ever be one quote popup in existance, remove all others
                document.querySelectorAll<HTMLElement>("#quote-selected-text-popup").forEach((elem) => elem.remove())

                const text = getSelectedBBCode(userName)
                console.log(text)
                if (text != null) {
                    createQuoteTextPopup(event.clientX, event.clientY + scrollY, text)
                }
            })
            postContent.setAttribute("data-mouseup-listener-added", "")
        }
    })
}

// Taken from https://stackoverflow.com/a/6668159 and modified heavily
function getSelectionHtml() {
    let html = ""
    if (typeof window.getSelection == "undefined") {
        return html
    }

    const sel = window.getSelection()
    if (sel == null) {
        return html
    }

    let outerContainer = createElement("div") // Outer container to store all of the nodes
    let container = createElement("div") // Inner container to append to

    let node = sel.getRangeAt(0).commonAncestorContainer

    // Get the parent node for any text nodes
    if (node.nodeName === "#text") {
        node = <Node>node.parentNode
    }

    // Special handling for box selections
    let boxBodyNode = getBoxSelection(outerContainer, node)

    let element = null
    if (node.nodeType == node.ELEMENT_NODE) {
        element = <Element>node.cloneNode()

        outerContainer.appendChild(element)
        container = <HTMLDivElement>element

        // If the selection parent was a box link, append the box body
        if (boxBodyNode) {
            outerContainer.appendChild(boxBodyNode)
        }
    }

    // Append all selected nodes to the new tree
    for (let i = 0, len = sel.rangeCount; i < len; ++i) {
        container.appendChild(sel.getRangeAt(i).cloneContents())
    }

    html = outerContainer.innerHTML

    return html
}

function getBoxSelection(outerContainer: HTMLDivElement, node: Node) {
    let boxBodyNode = null
    if (node.nodeName == "DIV" && (<HTMLDivElement>node).classList.contains("bbcode-spoilerbox__body")) {
        let boxLinkNode = <Node>node.previousSibling?.cloneNode(true)
        outerContainer.append(boxLinkNode)
    } else if (node.nodeName == "BUTTON" && (<HTMLDivElement>node).classList.contains("bbcode-spoilerbox__link")) {
        boxBodyNode = <Node>node.nextSibling?.cloneNode(true)
        // The body node needs to be appended after the link node...
    }

    return boxBodyNode
}

/**
 * get the currently selected part of the document as bbcode
 * @param username username of the user that is being quoted, used for the quote bbcode tag
 * @returns bbcode of document selection, null if nothing is selected
 */
function getSelectedBBCode(username: string) {
    let text = new BBCodeConverter(getSelectionHtml()).parse()

    if (text.trim() != "") {
        return `[quote="${username}"]${text}[/quote]`
    } else {
        return null
    }
}

/**
 * create the "Click to Quote" popup
 * @param x x position
 * @param y y postion
 * @param bbcodeText bbcode text to insert on click
 */
function createQuoteTextPopup(x: number, y: number, bbcodeText: string) {
    const quotePopup = createElement("div", {
        className: "qtip qtip-default qtip tooltip-default qtip-pos-bc",
        attributes: {
            id: "quote-selected-text-popup",
        },
        style: {
            display: "block",
            opacity: "1",
            position: "absolute",
            zIndex: "17000",
            cursor: "pointer",
            pointerEvents: "initial",
            top: `${y - 45}px`,
            left: `${x - 50}px`,
        },
        children: [
            createElement("div", {
                className: "qtip-tip",
                style: {
                    border: "0px !important",
                    display: "block",
                    width: "10px",
                    height: "8px",
                    lineHeight: "8px",
                    left: "50%",
                    marginLeft: "-5px",
                    bottom: "-8px",
                },
            }),
            createElement("div", {
                className: "qtip-content",
                attributes: {
                    textContent: "Click to Quote",
                },
            }),
        ],
        events: [
            {
                type: "click",
                handler: async () => {
                    insertTextIntoBBCodeEditor(bbcodeText)
                    quotePopup.remove()
                },
            },
        ],
    })
    document.body.append(quotePopup)
}
