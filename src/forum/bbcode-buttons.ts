import { ensuredSelector } from "../../typescript-lib/src/shared/common"
import { createElement, waitForElement } from "../../typescript-lib/src/shared/dom"
import { additionalBBCode } from "../constants/constants"
import { insertTextIntoBBCodeEditor } from "./index"

/**
 * This functions adds additional BBCode shortcut buttons to the text editor.
 * TODO: make it work with all editors (no clue why it doesnt currently)
 */
export async function insertAdditionalBBCodeButtons() {
    try {
        await waitForElement(".post-box-toolbar .bbcode-size-select")
    } catch (e) {
        return
    }

    document.querySelectorAll(".bbcode-editor__buttons-bar").forEach((editor) => {
        const target = ensuredSelector(".post-box-toolbar .bbcode-size-select")

        if (editor.querySelector(".btn-circle--bbcode.rr-lib") == null) {
            Object.entries(additionalBBCode).map((option) => {
                target.before(createBBCodeButton(option[0], option[1]))
            })
        }
    })
}

/**
 * This function creates a BBCode shortcut button based on given input.
 * @param name Name of the button/tag
 * @param icon Icon to be used (using FA icons)
 * @returns The button - HTMLButtonElement
 */
function createBBCodeButton(name: string, icon: string) {
    const button = createElement("button", {
        className: `btn-circle btn-circle--bbcode js-bbcode-btn--${name}`,
        attributes: {
            type: "button",
            title: name.charAt(0).toUpperCase() + name.slice(1),
        },
        children: [
            createElement("span", {
                className: `btn-circle__content`,
                children: [
                    createElement("i", {
                        className: icon,
                    }),
                ],
            }),
        ],
    })

    if (name == "color") {
        // @ts-ignore
        const colorPicker = createElement("input", {
            className: "color-picker",
            attributes: {
                type: "color",
                value: "#FF0000",
                onchange: () => insertBBCodeTagIntoTextArea(name, colorPicker.value),
            },
            style: {
                display: "none",
            },
        })

        document.body.append(colorPicker)

        button.addEventListener("click", () => {
            colorPicker.click()
        })
    } else {
        button.addEventListener("click", () => {
            insertBBCodeTagIntoTextArea(name)
        })
    }

    return button
}

/**
 * This function adds the tag to the text in the editor.
 * @param name Name of the tag
 * @param value Optional value (used for [color=])
 */
function insertBBCodeTagIntoTextArea(name: string, value?: string) {
    const textField = ensuredSelector<HTMLTextAreaElement>(".bbcode-editor__body")
    const selectionStart = textField.selectionStart
    const selectionEnd = textField.selectionEnd

    if (selectionStart == selectionEnd) {
        insertTextIntoBBCodeEditor(`[${name}${value != undefined ? `=${value}` : ""}][/${name}]`)
    } else {
        // value setting
        let selectedText = textField.value.slice(selectionStart, selectionEnd)
        textField.value = `${textField.value.substring(0, selectionStart)}[${name}${value != undefined ? `=${value}` : ""}]${selectedText}[/${name}]${textField.value.substring(selectionEnd, textField.value.length)}`
        textField.dispatchEvent(new Event("input", { bubbles: true }))

        // sets marked area
        textField.focus()
        textField.selectionStart = textField.value.substring(0, selectionStart).length + `[${name}${value != undefined ? `=${value}` : ""}]`.length
        textField.selectionEnd = textField.value.substring(0, selectionEnd).length + `[${name}${value != undefined ? `=${value}` : ""}]`.length
    }
}
