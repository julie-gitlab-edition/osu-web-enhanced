import { ModalWrapper } from "../../typescript-lib/src/components/modal/ModalWrapper"
import { ensuredSelector, wait } from "../../typescript-lib/src/shared/common"
import { createElement, createIcon, insertStyleTag } from "../../typescript-lib/src/shared/dom"
import { osuTheme } from "../../typescript-lib/src/shared/themes"
import { settings } from "../constants/constants"
import { CustomMessage } from "../constants/types"
import { useFeature } from "../settings"
import { insertParametersIntoCustomMessage } from "../template-messaging/parameters"
import { createMessageSelectModal } from "../template-messaging/views/MessageSelector"
import { insertAdditionalBBCodeButtons } from "./bbcode-buttons"
import { quoteSelectedText } from "./quote-text"

/**
 * This function handles all the initialisation action for the forum section of the script.
 */
export async function insertForumModifications() {
    do {
        useFeature(settings.forum.highlightOwnName.storageKey, insertOwnNameHighlighter)
        useFeature(settings.forum.showSendMessage.storageKey, insertSendMessageButton)
        useFeature(settings.forum.showAdditionalBBCode.storageKey, insertAdditionalBBCodeButtons)
        useFeature(settings.forum.insertQuoteAtCursorPosition.storageKey, insertQuoteAtCursorPosition)
        useFeature(settings.forum.showMessageManager.storageKey, insertForumTemplateButton)
        useFeature(settings.forum.quoteSelectedText.storageKey, quoteSelectedText)

        await wait(2500)
    } while (location.pathname.includes("forums"))
}

function insertOwnNameHighlighter() {
    if (document.querySelector("style.rr-lib") != null) {
        return
    }
    insertStyleTag(`
        .osu-page--forum a[href="${document.querySelector<HTMLAnchorElement>(".js-current-user-avatar")?.href}"],
        .osu-page--forum-topic a[href="${document.querySelector<HTMLAnchorElement>(".js-current-user-avatar")?.href}"] {
            color: ${osuTheme.successColor} !important;
            font-weight: 700 !important;
        }
    `)
}

/**
 * This function adds a button to every forum post to directly jump to the chat with the user.
 * Normally one would need to use the user context menu otherwise, which is a bit clunky.
 */
function insertSendMessageButton() {
    Array.from(document.querySelectorAll<HTMLElement>(".forum-post-info")).map((forumPost) => {
        // if there already is a send message button return
        if (forumPost.querySelector(".rr-lib.send-message-button") !== null) {
            return
        }
        try {
            const userID = forumPost.querySelector<HTMLAnchorElement>(".forum-post-info__row.forum-post-info__row--username.js-usercard")!.href.split("https://osu.ppy.sh/users/")[1]

            forumPost.querySelector(".forum-post-info__row--flag")?.append(
                createElement("a", {
                    className: "send-message-button",
                    attributes: {
                        href: `https://osu.ppy.sh/home/messages/users/${userID}`,
                        title: "Send message",
                    },
                    style: {
                        marginLeft: "8px",
                        backgroundColor: osuTheme.modalBackground,
                        borderRadius: "4px",
                        color: osuTheme.normalTextColor,
                        width: "30px",
                        height: "20px",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        boxShadow: "0 1px 3px rgba(0,0,0,.25)",
                    },
                    children: [createIcon("message")],
                })
            )
        } catch {}
    })
}

/**
 * Causes quote buttons on forum posts to insert their content at the current cursor position in the
 * topic reply box instead of at the end.
 */
function insertQuoteAtCursorPosition() {
    document.querySelectorAll<HTMLElement>(".js-forum-post").forEach((forumPost) => {
        let quoteButton = ensuredSelector<HTMLButtonElement>("div.forum-post__actions button.js-forum-topic-reply--quote", forumPost)

        if (quoteButton.dataset.clickListenerAdded == null) {
            quoteButton.addEventListener("click", async (event) => {
                event.stopImmediatePropagation()
                insertTextIntoBBCodeEditor(await (await fetch(`https://osu.ppy.sh/community/forums/posts/${forumPost.dataset.postId}/raw?quote=1`)).text())
            })
            quoteButton.setAttribute("data-click-listener-added", "")
        }
    })
}

/**
 * Inserts text into the texteditor
 * if cursor is a simple cursor => text at position
 * if cursor is selection => replace selection
 * @param text text to insert
 * @param editor editor to use, defaults to first bbcode_editor__body
 */
export function insertTextIntoBBCodeEditor(text: string, editor: string = ".bbcode-editor__body") {
    const textField = ensuredSelector<HTMLTextAreaElement>(editor)
    const selectionEnd = textField.selectionEnd
    const selectionStart = textField.selectionStart
    const cursorPosition = textField.value.substring(0, selectionEnd).length + text.length

    // value setting
    textField.value = textField.value.substring(0, selectionStart) + text + textField.value.substring(selectionEnd, textField.value.length)
    // update event + set cursor focus
    textField.dispatchEvent(new Event("input", { bubbles: true }))
    textField.focus()
    textField.selectionEnd = cursorPosition
    textField.selectionStart = cursorPosition
}

/**
 * inserts the button for forum customMessage modals
 */
function insertForumTemplateButton() {
    if (!location.pathname.includes("topics") || document.querySelector(".rr-lib.forum-templates") !== null) {
        return
    }

    insertStyleTag(`
    .forum-templates:hover{
        background-color: #382e32 !important
    }
    `)

    ensuredSelector(".bbcode-editor--reply .bbcode-editor__header").append(
        createElement("div", {
            className: "forum-templates",
            children: [createIcon("collection")],
            attributes: {
                title: "Forum Post Templates",
                onclick: () => {
                    document.body.append(new ModalWrapper(createMessageSelectModal("forum")))
                },
            },
            style: {
                padding: "4px",
                backgroundColor: "#46393f",
                cursor: "pointer",
                display: "flex",
                borderRadius: "4px",
            },
        })
    )
}

/**
 * inserts a customMessage into the BBCode Editor
 * @param customMessage customMessage to insert
 */
export function insertForumMessage(customMessage: CustomMessage) {
    insertTextIntoBBCodeEditor(insertParametersIntoCustomMessage(customMessage), "div:not(.js-forum-post) textarea.bbcode-editor__body")
}
