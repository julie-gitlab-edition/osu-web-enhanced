import { createElement } from "../../typescript-lib/src/shared/dom"
import { osuTheme } from "../../typescript-lib/src/shared/themes"

/**
 * This object defines the character counter for the custom message editor.
 */
export class MessageEditorCharacterCount extends HTMLDivElement {
    /**
     * The span element holding the changing count
     */
    private counter: HTMLSpanElement

    /**
     * This number defines the maximum length of a message, defined by the chat itself.
     */
    private maxMessageLength: number = 449

    /**
     * constructor
     * @param count starting count
     */
    constructor(count: number) {
        super()

        this.counter = createElement("span", { attributes: { innerText: count.toString() } })
        const counterRight = createElement("span", { attributes: { innerText: `/${this.maxMessageLength.toString()}` } })
        this.style.color = osuTheme.normalTextColor
        this.style.fontSize = "14px"
        this.style.minWidth = "52px"

        this.append(this.counter, counterRight)
    }

    /**
     * This method updates the character counter to the given number. If the number exceed the maximum message length the counter will be turned red.
     * @param count length of the custom message.
     */
    public updateCounter(count: number) {
        this.counter.innerText = count.toString()

        if (count > this.maxMessageLength) {
            this.style.color = osuTheme.failureColor
        } else {
            this.style.color = osuTheme.normalTextColor
        }
    }
}
