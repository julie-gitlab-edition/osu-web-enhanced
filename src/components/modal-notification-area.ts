import { osuTheme } from "../../typescript-lib/src/shared/themes"
import { notificationType } from "../constants/types"

/**
 * This object defines the notification area of the custom chat message editor.
 * It can have various different status. @see notificationType
 */
export class ModalNotificationArea extends HTMLDivElement {
    /**
     * constructor
     * @param startingType defines the notificationType for the initial creation
     * @param message defines the message for the initial creation
     */
    constructor(startingType: notificationType, message: string) {
        super()

        this.innerText = message
        if (startingType === "danger") {
            this.style.color = osuTheme.failureColor
        } else if (startingType === "normal") {
            this.style.color = osuTheme.normalTextColor
        }

        this.style.width = "100%"
        this.style.padding = "4px"
        this.style.backgroundColor = osuTheme.secondaryBackground
        this.style.borderRadius = "4px"
        this.style.fontSize = "14px"
    }

    /**
     * This method updates the notification and status type.
     * @param type the type of the status update. @see notificationType
     * @param message the message the status should display.
     */
    public updateMessage(type: notificationType, message: string) {
        this.innerText = message
        if (type === "danger") {
            this.style.color = osuTheme.failureColor
        } else if (type === "normal") {
            this.style.color = osuTheme.normalTextColor
        }
    }
}
