import { OsuModal } from "../../typescript-lib/src/components/modal/osu/OsuModal"
import { CustomMessageParameterInput } from "../components/message-editor-parameter-input"
import { CustomMessage, Parameter } from "../constants/types"

/**
 * Formats a customMessage with its parameters
 * @param customMessage selected message @see CustomMessage
 * @returns formatted message as a string
 */
export function insertParametersIntoCustomMessage(customMessage: CustomMessage) {
    let chatMessage = customMessage.messageOptions.message

    if (customMessage.messageOptions.parameters !== undefined) {
        customMessage.messageOptions.parameters.forEach((p: Parameter) => {
            const value = window.prompt(`Please insert:\n${p.label}`)
            if (value == null) {
                throw new Error("Input Cancelled")
            } else if (value == "") {
                window.alert("Empty input: please enter something!")
                throw new Error("Input Empty")
            } else {
                chatMessage = chatMessage.replace(p.parameter, value)
            }
        })
    }

    return chatMessage
}

/**
 * Gets the created parameters from the supplied modal.
 * @param modal modal
 * @returns parameters as Parameter[]
 */
export function getParametersFromInputs(modal: OsuModal) {
    return [...Array.from(modal.querySelectorAll<CustomMessageParameterInput>(".parameter-input")).map((p) => p.getParameter())]
}

/**
 * Checks that all input fields are filled out
 * @param modal modal to check for valid inputs
 * @returns true if all are filled, false if not
 */
export function checkParameterInputState(modal: OsuModal) {
    return Array.from(modal.querySelectorAll<CustomMessageParameterInput>(".parameter-input"))
        .map((p) => p.checkInputState())
        .every((e) => e == true)
}
